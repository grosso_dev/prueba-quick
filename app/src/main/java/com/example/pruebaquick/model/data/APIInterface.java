package com.example.pruebaquick.model.data;

import com.example.pruebaquick.model.entity.ResultApi;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {

    @GET("api/character/?page=1")
    Call<ResultApi> getChararacters();
}
