package com.example.pruebaquick.model.entity;

import java.util.List;

public class Result {

    public Integer id;
    public String name;
    public String status;
    public String species;
    public String type;
    public String gender;
    public Origin origin;
    public Location location;
    public String image;
    public List<String> episode = null;
    public String url;
    public String created;

}