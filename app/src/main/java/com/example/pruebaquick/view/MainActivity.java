package com.example.pruebaquick.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pruebaquick.R;
import com.example.pruebaquick.model.data.APIClient;
import com.example.pruebaquick.model.data.APIInterface;
import com.example.pruebaquick.model.entity.Result;
import com.example.pruebaquick.model.entity.ResultApi;
import com.example.pruebaquick.utils.ServiceAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    APIInterface mApiInterface;
    RecyclerView mRecyclerView;
    private List<Result> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();
        setAdapter();

    }


    //se instancian los componentes de la vista
    private void setView() {
        mRecyclerView= (RecyclerView) findViewById(R.id.recycler_view);
    }

    // se hace llamado al servicio y se le carga la información al adaptador
    public void setAdapter() {
        mApiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResultApi> call = mApiInterface.getChararacters();
        call.enqueue(new Callback<ResultApi>() {
            @Override
            public void onResponse(Call<ResultApi> call, final Response<ResultApi> response) {
                if (response.isSuccessful()) {
                    data = response.body().results;
                    ServiceAdapter adapter = new ServiceAdapter(data,getApplicationContext());
                    mRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));
                    adapter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Result result =  response.body().results.get(mRecyclerView.getChildAdapterPosition(view));
                            String description = "Selecciono: "+result.name + " Genero: "+ result.gender + " Especie: "+
                                    result.species;
                            Toast.makeText(getApplicationContext(),description ,Toast.LENGTH_SHORT).show();
                        }
                    });
                    mRecyclerView.setAdapter(adapter);
                } else {
                    Toast.makeText(MainActivity.this, "Error en la estructura del servicio", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultApi> call, Throwable t) {
                Toast.makeText(MainActivity.this, "No se puede conectar al servicio", Toast.LENGTH_SHORT).show();
            }
        });



    }
}
