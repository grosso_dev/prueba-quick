package com.example.pruebaquick.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pruebaquick.R;
import com.example.pruebaquick.model.entity.Result;
import com.example.pruebaquick.model.entity.ResultApi;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter
        extends RecyclerView.Adapter<ServiceAdapter.ViewHolderService>
        implements View.OnClickListener{

    List<Result> dataList;
    private View.OnClickListener listener;
    private Context context;

    public ServiceAdapter(List<Result> getData, Context _context) {
        this.dataList = getData;
        this.context = _context;
    }

    @Override
    public ViewHolderService onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,null,false);
        view.setOnClickListener(this);
        return new ViewHolderService(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderService holder, int position) {
        holder.name.setText(dataList.get(position).name);
        // se implementa la libreria de picasso para previsualizar las imagenes en el adaper
        Picasso.with(context).load(dataList.get(position).image).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View view) {
        if (listener!=null){
            listener.onClick(view);
        }
    }

    public class ViewHolderService extends RecyclerView.ViewHolder {

        TextView name;
        ImageView imageView;

        public ViewHolderService(View itemView) {
            super(itemView);
            name= (TextView) itemView.findViewById(R.id.person_name);
            imageView= (ImageView) itemView.findViewById(R.id.image_service);

        }
    }
}